<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login Form</title>
    <style type="text/css">
        body {
            text-align: center;
        }
        .main {
            margin: auto;
            text-align: left;
            display: inline-block;
            border: 1px solid blue;
            padding: 25px 50px;
        }
        .label {
            background-color: #0088e3;
            border: 1px solid #005c99;
            padding: 10px;
            color: white;
            margin: 10px 0 0 0;
        }
        .input {
            border: 1px solid #005c99;
            padding: 10px;
            margin: 10px 0 0 0;
        }
    </style>
</head>
<body>
    <?php date_default_timezone_set('Asia/Ho_Chi_Minh'); ?>
    <?php echo "
        <div class=\"main\">
            <table>
                <tr>
                    <td colspan=\"2\">
                        <div style=\"background-color: #d1d1d1; padding: 10px;\">Bây giờ là: " . date("H:i d/m/Y") . "</div>
                    </td>
                </tr>
                <tr>
                    <td><div class=\"label\">Tên đăng nhập</div></td>
                    <td><input class=\"input\" type='text' name='username'></td>
                </tr>
                <tr>
                    <td><div class=\"label\">Mật khẩu</div></td>
                    <td><input class=\"input\" type='text' name='username'></td>
                </tr>
                <tr>
                    <td colspan=\"2\">
                        <button style=\"
                            margin: 15px auto 0;
                            display: block;
                            padding: 15px 30px; 
                            background-color: #0088e3; 
                            border: 1px solid #005c99; 
                            border-radius: 10px;
                            color: white;
                            \" type='submit'>
                                Đăng nhập
                        </button>
                    </td>
                </tr>
            </table>
        </div>
    ";
    ?>
</body>
</html>